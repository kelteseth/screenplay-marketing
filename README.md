# ScreenPlay Marketing

This project is for everything related to marketing.

1. [Launch Trailer](trailer_launch/README.md)


### Contribute

1. Fork this repo with your gitlab.com account
2. Download this repo via git.
   1. Install [git](https://git-scm.com/)
      * You can install git with the default settings!
   2. **IMPORTANT** after you have git installed reopen and shell/cmd/powershell and type
      * 'git lfs install'
   2. Install a git gui [GitExtentions](https://gitextensions.github.io/)
      * You need to setup your gitlab email and username after the installation!
      * As a merge tool is [vscode (free)](https://code.visualstudio.com/) the best
3. Select the blue Clone button from your forked ScreenPlay and select the "Clone with HTTPS" url
4. Download via  GitExtentions: 
    1. Start -> Clone repository
    2. Enter your forked repo https://gitlab.com/**YOUR_NAME**/screenplay-marketing.git

